import logo from "./logo.svg";
import "./App.css";
import Login from "./pages/login";
import ThemeProvider, { ThemeContext } from "./utils/theme.context";
import DetailUser from "./pages/detail_user";
import { Provider } from "react-redux";
import {
  BrowserRouter,
  Routes,
  Route,
  Outlet,
  useNavigate,
  useLocation,
  Navigate,
} from "react-router-dom";
import store from "./store";
import "antd/dist/antd.css";
import Users from "./pages/detail_user/user_list";

function App() {
  return (
    <Provider store={store}>
      <ThemeProvider _theme="dark">
        <div className="App">
          <BrowserRouter>
            <Routes>
              <Route path="/login/" element={<Login />} />
              <Route path="/" element={<PrivateRoute />}>
                <Route path="/user/:user_id/" element={<DetailUser />} />
                <Route path="/" element={<Users />} />
              </Route>
              <Route
                path="*"
                element={
                  <main style={{ padding: "1rem" }}>
                    <p>404 Esa vaina no existe</p>
                  </main>
                }
              />
            </Routes>
          </BrowserRouter>
        </div>
      </ThemeProvider>
    </Provider>
  );
}

export default App;

const PrivateRoute = () => {
  const nav = useNavigate();
  const location = useLocation();
  const canAccess = !!localStorage.getItem("token");
  return (
    <div>
      <button
        className="btn btn-primary"
        onClick={() => {
          localStorage.removeItem("token");
          nav("/login/");
        }}
      >
        Logout
      </button>
      {canAccess ? (
        <Outlet />
      ) : (
        <Navigate to="/login/" replace state={{ url: location.pathname }} />
      )}
    </div>
  );
};
