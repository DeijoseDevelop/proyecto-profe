import axios from "axios"

export const general = axios.create({
    baseURL: "https://reqres.in/api",
    timeout: 1000
});

// general.interceptors.response.use(null, (error) => {
//     console.log({...error});
//     if(error.response){
//         switch (error.response.status) { 
//             case 404:
//                 const config = error.response.config;
//                 config.url = "/users/1";
//                 return axios.request(config);
//             default:
//                 return Promise.reject(error)
//         }
//     }else {
//         return Promise.reject("Error de Coneccion", error)
//     }
// })