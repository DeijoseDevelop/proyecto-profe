import { general } from "./intance"

export const login = async({email, password}) => {
    try {
        const res = await general.post("/login", {
            email,
            password
        })
        return res.data.token;
    }catch(e) {
        return Promise.reject(console.error());
    }
} 