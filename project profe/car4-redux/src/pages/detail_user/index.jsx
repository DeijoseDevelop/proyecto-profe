import React, { useContext, useEffect } from "react";

import { useDispatch, useSelector } from "react-redux";

import { useNavigate, useParams, useSearchParams } from "react-router-dom";
import { getUserById } from "./redux/actions";
import { Avatar, Card, Switch } from "antd";
import {
  EditOutlined,
  EllipsisOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import { ThemeContext } from "../../utils/theme.context";

const { Meta } = Card;

const DetailUser = () => {
  const params = useParams();
  const dispatch = useDispatch();
  const nav = useNavigate();
  const [q] = useSearchParams();
  const context = useContext(ThemeContext);
  
  const name = q.getAll('name')
  console.log({name});


  const text_class =
    context.theme === "dark" ? "text-white text-center" : "text-center";

  const [user, error, loading] = useSelector((store) => [
    store.detail_user.user.value,
    store.detail_user.user.error,
    store.detail_user.user.loading,
  ]);

  console.log(params);
  useEffect(() => {
    dispatch(getUserById(params.user_id));
  }, []);

  const onChange = () => {
    context.toggle_theme();
  };

  return (
    <div className={[`bg-${context.theme}`, "App p-5"].join(" ")}>
      <Switch checkedChildren="dark" unCheckedChildren="ligth" className="mb-5" checked={context.theme === "dark"} onChange={onChange} />
      <div>
        
        {!loading && (
          <div>
            {error && <span className="text-danger">Hola {error}</span>}
            {user && (
              <Card
                hoverable
                style={{ width: 240 }}
                cover={
                  <img src="https://picsum.photos/240/180" alt={user.email} />
                }
                actions={[
                  <SettingOutlined key="setting" />,
                  <EditOutlined key="edit" />,
                  <EllipsisOutlined key="ellipsis" />,
                ]}
              >
                <Meta
                  avatar={<Avatar size="large" src={user.avatar} />}
                  title={`${user.first_name} ${user.last_name}`}
                  description={user.email}
                />
              </Card>
            )}
          </div>
        )}
        {loading && <span className={text_class}>Cargando ... </span>}
      </div>
    </div>
  );
};

export default DetailUser;
