import types from "./types";
import { getUserById as getUser, getUsersByPage } from "./services";

export const getUserById = (id) => {
  return async (dispatch, getState) => {
    const store = getState();
    const actual_user = store.detail_user.user.value;
    if (actual_user?.id !== id) {
      dispatch({ type: types.GetUserByID });
      return await getUser(id)
        .then((res) => {
          dispatch(GetUserByIDSuccess(res));
          return res;
        })
        .catch((error) => {
          if (error.response) {
            if (error.response.status === 404) {
              const e = `El usuario ${id} no existe`;
              dispatch(GetUserByIDFail(e));
              return e;
            }
          }
          return error;
        });
    }
  };
};

export const GetUserByIDSuccess = (res) => {
  return {
    type: types.GetUserByIDSuccess,
    payload: res,
  };
};

export const GetUserByIDFail = (error) => {
  return {
    type: types.GetUserByIDFail,
    payload: error,
  };
};

export const getUsers = (page = 1) => {
  return (dispatch) => {
    dispatch({ type: types.GetUsersByPage });
    return getUsersByPage(page)
      .then((res) => {
        dispatch({ type: types.GetUsersByPageSuccess, payload: res });
        return res;
      })
      .catch((error) => {
        const e = "Error al obtener el usuario";
        dispatch({ type: types.GetUsersByPageFail, payload: e });
        return error;
      });
  };
};

export default {
  getUserById,
  getUsers,
};
