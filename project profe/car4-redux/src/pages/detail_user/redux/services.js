import { general } from "../../../service/intance";

// https://reqres.in/api/users/2
export const getUserById = async (id) => {
  try {
    const res = await general.get(`/users/${id}`);
    // aqui se puede modificar la respuesta
    return res.data.data;
  } catch (error) {
    return Promise.reject(error);
  }
};

// https://reqres.in/api/users?page=1
export const getUsersByPage = async (page) => {
  try {
    const res = await general.get(`/users`, {
      params: {
        page: page,
      },
    });
    // aqui se puede modificar la respuesta
    return res.data;
  } catch (error) {
    return Promise.reject(error);
  }
};
