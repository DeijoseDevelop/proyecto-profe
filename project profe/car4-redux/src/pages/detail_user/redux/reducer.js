import types from "./types";

const initial_state = {
  user: {
    value: null,
    loading: false,
    loaded: false,
    error: "",
  },
  users: {
    value: null,
    loading: false,
    loaded: false,
    error: "",
  },
};

const reducer = (state = initial_state, action) => {
  switch (action.type) {
    case types.GetUserByID:
      return {
        ...state,
        user: {
          ...state.user,
          loaded: false,
          loading: true,
        },
      };
    case types.GetUserByIDSuccess:
      return {
        ...state,
        user: {
          ...state.users,
          value: action.payload,
          loaded: true,
          loading: false,
        },
      };
    case types.GetUserByIDFail:
      return {
        ...state,
        user: {
          ...state.users,
          error: action.payload,
          value: null,
          loaded: false,
          loading: false,
        },
      };

    case types.GetUsersByPage:
      return {
        ...state,
        users: {
          ...state.users,
          loaded: false,
          loading: true,
        },
      };
    case types.GetUsersByPageSuccess:
      return {
        ...state,
        users: {
          ...state.user,
          value: action.payload,
          loaded: true,
          loading: false,
        },
      };
    case types.GetUsersByPageFail:
      return {
        ...state,
        users: {
          ...state.user,
          error: action.payload,
          value: null,
          loaded: false,
          loading: false,
        },
      };

    default:
      return state;
  }
};

export default reducer;
