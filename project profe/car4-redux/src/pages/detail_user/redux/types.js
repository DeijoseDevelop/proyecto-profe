const types = {
    
    GetUserByID: "[detail_user] get user by id",
    GetUserByIDSuccess: "[detail_user] get user by id success",
    GetUserByIDFail: "[detail_user] get user by id fail",
    
    GetUsersByPage: "[detail_user] get users by page",
    GetUsersByPageSuccess: "[detail_user] get users by page success",
    GetUsersByPageFail: "[detail_user] get users by page fail",
}

export default types;