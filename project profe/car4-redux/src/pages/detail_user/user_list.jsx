import React, { useContext, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import { getUserById, getUsers } from "./redux/actions";
import { Avatar, Card, Switch } from "antd";
import {
  EditOutlined,
  EllipsisOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import { ThemeContext } from "../../utils/theme.context";

const { Meta } = Card;

const Users = () => {
  const dispatch = useDispatch();
  const nav = useNavigate();
  const context = useContext(ThemeContext);
  const location = useLocation();

  const [users, page, total_pages, loading, error] = useSelector((store) => {
    const value = store.detail_user.users.value;
    return [
      value ? value.data : [],
      value ? value.page : 1,
      value ? value.total_pages : 0,
      store.detail_user.users.loading,
      store.detail_user.users.error,
    ];
  });

  const text_class =
    context.theme === "dark" ? "text-white text-center" : "text-center";

  useEffect(() => {
    dispatch(getUsers());
  }, []);

  const onChange = () => {
    context.toggle_theme();
  };
  const next = page + 1;
  const prev = page - 1;


  return (
    <div className={[`bg-${context.theme}`, "App p-5"].join(" ")}>
      <Switch
        checkedChildren="dark"
        unCheckedChildren="ligth"
        className="mb-5"
        checked={context.theme === "dark"}
        onChange={onChange}
      />
      <div>
        {/* <button
          className="btn btn-primary"
          onClick={() => {
            localStorage.removeItem("token");
            nav("/");
          }}
        >
          Logout
        </button> */}
        {!loading && (
          <div className="d-flex justify-content-center flex-column ">
            {error && <span className="text-danger">Hola {error}</span>}
            {users && users.length > 0 && (
              <div className="row" style={{ width: "1000px" }}>
                {users.map((user, index) => (
                  <div className="col-4 mb-3 " key={`user-${index}`}>
                    <Card
                      hoverable
                      style={{ width: 240 }}
                      cover={
                        <img
                          src={`https://picsum.photos/240/180?ramdom=${
                            index + page
                          }`}
                          height={180}
                          alt={user.email}
                        />
                      }
                      actions={[
                        <SettingOutlined key="setting" />,
                        <EditOutlined key="edit" />,
                        <EllipsisOutlined key="ellipsis" />,
                      ]}
                      onClick={() => {
                        nav(`/user/${user.id}/`, { state: user });
                      }}
                    >
                      <Meta
                        avatar={<Avatar size="large" src={user.avatar} />}
                        title={`${user.first_name} ${user.last_name}`}
                        description={user.email}
                      />
                    </Card>
                  </div>
                ))}
              </div>
            )}
            <div>
              <button
                className="btn btn-primary me-2"
                disabled={prev <= 0}
                onClick={() => dispatch(getUsers(prev))}
              >
                Anterior
              </button>
              <button
                className="btn btn-primary"
                disabled={next > total_pages}
                onClick={() => dispatch(getUsers(next))}
              >
                Sigiente
              </button>
            </div>
          </div>
        )}
        {loading && "Cargando ... "}
      </div>
    </div>
  );
};

export default Users;
