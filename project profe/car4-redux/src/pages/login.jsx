import { Switch } from "antd";
import React, { useContext, useRef } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import LoginForm from "../components/login-form";
import { login } from "../service/auth";
import { ThemeContext } from "../utils/theme.context";

const Login = () => {
  const ref = useRef(null);
  const nav = useNavigate();
  const location = useLocation();
  const context = useContext(ThemeContext);

  console.log(location);

  const _login = (a) => {
    login(a).then((token) => {
      localStorage.setItem("token", token);
      nav("/", { state: location });
    });
  };

  let ds = false;
  if (ref.current) {
    ds = ref.current.isSubmitting;
  }

  const onChange = () => {
    context.toggle_theme();
  };
  const text_class =
    context.theme === "dark" ? "text-white text-center" : "text-center";

  return (
    <div className="App p-5">
      <Switch
        checkedChildren="dark"
        unCheckedChildren="ligth"
        className="mb-5"
        checked={context.theme === "dark"}
        onChange={onChange}
      />
      <div className="w-100 h-100 d-flex align-items-center justify-content-center">
        <div className={`container-fluid bg-${context.theme}`}>
          <div className="row">
            <div
              className="col-6"
              style={{
                backgroundSize: "cover",
                backgroundImage: "url(https://picsum.photos/800/300)",
              }}
            ></div>
            <div className="col-6 p-5">
              <h1 className={text_class}>Projecto CAR IV</h1>
              <LoginForm onSubmit={_login} myRef={ref} />
              <button
                className="btn btn-primary w-100"
                disabled={ds}
                onClick={() => {
                  console.log("ok", ref.current);
                  if (ref.current) {
                    ref.current.submitForm();
                  }
                }}
              >
                Login
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
