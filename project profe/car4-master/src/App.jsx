import logo from "./logo.svg";
import "./App.css";
import Login from "./pages/login";
import ThemeProvider from "./utils/theme.context";

function App() {
  return (
    <ThemeProvider _theme="light">
      <div className="App">
        <Login />
      </div>
    </ThemeProvider>
  );
}

export default App;
